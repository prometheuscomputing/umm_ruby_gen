# TODO
# - Does there need to be some checking that no two constants have the same name?  I don't think so.
require 'pp'
require 'rainbow'
require 'uml_metamodel'
require 'appellation'
require_relative 'type_mapping'

module UmlMetamodel
  class ElementBase
    attr_reader :implementation
    def prepare_ruby_gen(builder)
      # puts "Preparing #{self.class} -- #{qualified_name}"
      builder.register(self)
    end
    def create_ruby
      # TODO maybe change this to only give a warning
      # FIXME not everything calls #super
      raise "There is already an implementation of #{qualified_name}, which indicates that #create_ruby has already been called.  You can only call this methods once." if @implementation
    end
  end
  
  class Element < ElementBase  
    def create_ruby
      super
    end
    def init_orient
      super
    end
  end
  
  class Project < ElementBase
    def prepare_ruby_gen(builder)
      contents.each { |e| e.prepare_ruby_gen(builder) }
    end
    def create_ruby
      super
      # Project is going to be a top-level constant (i.e. defined on Object)
      # There isn't really any functionality associated with the modules that get created for the project.  Maybe it shouldn't be done at all?
      if Object.const_defined?(qualified_name)
        mod = qualified_name.to_const
      else
        mod = Object.const_set(name, Module.new)
        mod.const_set('UML', self)
      end
      @implementation = mod
      mod.module_eval do
        class << self
          attr_accessor :info
        end
        @info = {}
        def self.uml
          const_get('UML')
        end
        def self.included(base)
          raise "#{self.name} is not a mixin module.  Do not include it!"
        end
      end
      mod
    end
    def init_orient
      super
    end
  end
  
  class Package < Element
    
    def prepare_ruby_gen(builder)
      super
      contents.each { |e| e.prepare_ruby_gen(builder) }
    end
    
    def create_ruby
      base = package ? package.qualified_name.to_const : Object
      if base.const_defined?(name)
        mod = qualified_name.to_const
      else
        mod = base.const_set(name, Module.new)
        mod.const_set('UML', self)
      end
      @implementation = mod
      mod.module_eval do
        class << self
          attr_accessor :info
        end
        @info = {}
        def self.uml
          const_get('UML')
        end
        def self.included(base)
          raise "#{self.name} is not a mixin module.  Do not include it!"
        end
      end
      mod
    end
    
    def init_orient
      super
    end
  end
  
  class Classifier < Element
    def prepare_ruby_gen(builder)
      super
      contents.each { |e| e.prepare_ruby_gen(builder) } # Not sure what this will actually get.  Inner classes?
      properties.each { |e| e.prepare_ruby_gen(builder) } unless primitive? # while primitives may have properties that take part in associations, they do not get implemented
    end
    
    def create_ruby
      # NOTE -- multiple inheritance is not currently supported
      parent = ::Object
      if parents.any?
        parent_name = parents.first.qualified_name
        parent = RubyBuilder.primitive_mapping[parent_name] || parent_name.to_const
      end
      base = package ? package.qualified_name.to_const : ::Object
      # FIXME we are potentially moving things out of the package that the UmlMetamodel DSL file says they are in here.  Classes like File, Date, Time are already defined on object.  Do we want to do this??  We are also potentially adding properties to these classes, e.g. File is a "complex attribute" in GuiBuilderProfile (what that really means is that it is a compound data type).  It seems a bit unsafe to define properties on things like ::File
      if Object.const_defined?(qualified_name)
        puts Rainbow("#{qualified_name} is defined on Object and is specified with #{base}").orange
      else
        base.const_set(name, ::Class.new(parent))
      end
      mod = qualified_name.to_const
      mod.const_set('UML', self)
      
      common_ruby_setup(mod)
      mod.module_eval do
        def self.properties
          props = {}
          implements.each { |iface| props.merge!(iface.properties) }
          props.merge!(@info[:properties])
          # TODO multiple inheritance? Well, we did it here and it is gross.  Totally sketchy what the order of inheritance should be.  I really don't like multiple inheritance.
          # superklass = uml.parents.first.implementation
          if uml.parents.any?
            parent_props = {}
            uml.parents.each { |parent| parent_props.merge!(parent.implementation.properties) }
            props = parent_props.merge(props)
          else
            props
          end
          props
        end
        def self.implements
          []
        end
        # prepend PersistanceBehavior # e.g. functionality that makes this thing work with a graph database
      end
      mod
    end
    
    private
    def common_ruby_setup(mod)
      @implementation = mod
      mod.module_eval do
        class << self
          attr_accessor :info
        end
        @info = {:properties => {}}
        def self.add_property(property_key, property_data)
          attr_accessor(property_key)
          if @info[:properties][property_key]
            @info[:properties][property_key].merge!(property_data)
          else
            @info[:properties][property_key] = property_data
          end
        end
        singleton_class.send(:alias_method, :update_property, :add_property)
        def self.attributes
          properties.select { |_,info| info[:attribute] }
        end
        def self.associations
          properties.select { |_,info| info[:association] }
        end
        def self.uml
          const_get('UML')
        end
      end
    end
  end

  class Class < Classifier
    def create_ruby
      mod = super
      mod.module_eval do
        def self.implements
          # ancestors.take_while {|a| a != superclass }.select {|ancestor| ancestor.instance_of?(Module) }
          # puts Rainbow(uml.implements.inspect).cyan
          uml.implements.collect(&:implementation)
        end
      end
      implements.each do |uml_iface|
        iface = uml_iface.qualified_name.to_const
        mod.include iface
      end
      mod
    end    
  end
  
  class Interface < Classifier
    def create_ruby
      base = package ? package.qualified_name.to_const : Object
      if base.const_defined?(name)
        mod = qualified_name.to_const
      else
        mod = base.const_set(name, Module.new)
        mod.const_set('UML', self)
      end
      common_ruby_setup(mod)
      mod.module_eval do
        def self.properties
          info[:properties]
        end
        def self.included(base)
          raise "#{base.name} is a datatype and may not realize an interface." if uml.is_a?(UmlMetamodel::Datatype)
        end
        def self.implements
          # ancestors.reject { |a| a == self }.reverse
          uml.parents.collect(&:implementation)
        end
      end
      parents.each { |parent| mod.include parent.qualified_name.to_const }
      mod
    end
  end
  
  # class Primitive < Datatype
  # end
  
  # class Datatype < Classifier
  # end
  
  # class Enumeration < Datatype
  # end
  
  class Property < Element
    def prepare_ruby_gen(builder)
    end
    
    def create_ruby
      o = owner.qualified_name.to_const
      n = (name || Appellation.role(self)).to_sym
      
      # TODO
      # :display_name=>"Name",
      # :additional_data can now be derived from #uml
      
      # TODO should we be mapping the type if it is a primitive?
      typ = RubyBuilder.primitive_mapping[type.qualified_name] || type.qualified_name
      o.add_property(n, {:getter => n, :uml => self, :class => typ, :as => owner.qualified_name})
      if association
        o.properties[n][:association] = true
        this_end, opp_end = association.properties.sort_by { |prop| prop == self ? 0 : 1 }
        raise unless this_end == self
        if self.upper > 1
          if opp_end.upper > 1
            o.properties[n][:type] = :many_to_many
          else
            o.properties[n][:type] = :one_to_many
          end
        else
          if opp_end.upper > 1
            o.properties[n][:type] = :many_to_one
          else
            o.properties[n][:type] = :one_to_one
          end
        end
      else
        o.properties[n][:attribute] = true
      end
      o # returning the owner of the property -- what else would we return??
    end
  end
  
  class Association < Element
    def prepare_ruby_gen(builder, opts = {})
      super(builder)
    end
    def create_ruby
      # well, what do we do here I wonder....
    end
  end
  
  # class EnumerationLiteral < Element
  # end
end
