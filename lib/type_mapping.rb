require 'date'
require 'uri'
class RubyBuilder < UMMDrivenBuilder
  def self.primitive_mapping(reload = false)
    @umm_to_ruby_primitive_mapping ||= {}
    @umm_to_ruby_primitive_mapping = {} if reload
    return @umm_to_ruby_primitive_mapping unless @umm_to_ruby_primitive_mapping.empty?
    
    @umm_to_ruby_primitive_mapping['UML::Integer'] = ::Integer
    @umm_to_ruby_primitive_mapping['UML::Real'] = ::Float
    @umm_to_ruby_primitive_mapping['UML::String'] = ::String
    # FIXME there is no such thing as a Boolean type in Ruby.  What to do?
    @umm_to_ruby_primitive_mapping['UML::Boolean'] = 'Boolean'
    @umm_to_ruby_primitive_mapping['UML::UnlimitedNatural'] = ::Integer

    # FIXME I'm not entirely sure whether byte should really be an Integer or a String
    @umm_to_ruby_primitive_mapping['UML::ByteString'] = ::String
    @umm_to_ruby_primitive_mapping['UML::Char'] = ::String
    @umm_to_ruby_primitive_mapping['UML::Date'] = ::Date
    @umm_to_ruby_primitive_mapping['UML::Float'] = ::Float
    @umm_to_ruby_primitive_mapping['UML::Null'] = ::NilClass
    @umm_to_ruby_primitive_mapping['UML::Datetime'] = ::DateTime
    @umm_to_ruby_primitive_mapping['UML::Time'] = ::Time
    # TODO URI::Generic is a tricky thing...
    @umm_to_ruby_primitive_mapping['UML::Uri'] = ::URI::Generic
    @umm_to_ruby_primitive_mapping['UML::RegularExpression'] = ::Regexp

    @umm_to_ruby_primitive_mapping
  end
end
