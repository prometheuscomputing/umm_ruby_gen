# require 'ruby_gen/requirements'
# require_relative 'type_map'
# load 'ruby_gen/uml_metamodel/to_ruby.rb'
require 'rainbow'
require 'umm_driven_builder'
require_relative 'ruby_gen'
require 'common/constants'

class RubyBuilder < UMMDrivenBuilder
  
  TEMPLATE_DIR = File.join(File.expand_path(File.dirname(__FILE__)), "templates")
  
  def self.generate_ruby_from_umm_project(project)
    new(project).build
  end
    
  def initialize(project)
    super
  end

  def build
    @project.prepare_ruby_gen(self)
    # puts @registry.pretty_inspect
    order_packages
    order_classifiers
    @ordered_elements.each do |e|
      e.create_ruby if e.respond_to?(:create_ruby)
    end
    @ordered_elements.each do |e|
      e.properties.each { |prop| prop.create_ruby } if e.is_a?(UmlMetamodel::Classifier)
    end
  end
end