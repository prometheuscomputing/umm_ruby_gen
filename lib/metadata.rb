module ModelMetadata # could collide with existing...
  module Classifier
    def properties
      # This set_up could be cached if the uml elements are frozen.  There is no guarantee that they are and are conceivable use cases for them to manipulated during runtime.
      uml.all_properties.collect { |prop| [prop.name.to_sym, {:getter}]}.to_h
    end
  end
  module Package
  end
end