# Need to test to see if a query to get all of a given type of element will retrieve embedded instances of the element or only free-standing instances of the element.  If we don't get the embedded ones then I think we will want to not use links and instead use edges.

module IFoo
  ZIP = 'IFooZip'
  attr_accessor :ibar
  def zum
    'ifoozum'
  end
end
class Foo
  def self.add_attr val
    attr_accessor val.to_sym
  end
end
Foo.add_attr 'clug'
f = Foo.new
f.clug = 'clug'
f.clug



f.zum
Foo::ZIP
IFoo::ZIP
Foo.zip