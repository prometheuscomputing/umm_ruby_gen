#!/usr/bin/env ruby
if ARGV[0] =~ /test|dev|local/i
  ARGV.shift
  require 'lodepath'
  LodePath.amend
  LodePath.display
end
require 'uml_metamodel'
require_relative '../lib/ruby_builder'

path = File.expand_path('~/projects/uml_metamodel/test_data/CarExampleApplication.rb')
uml  = UmlMetamodel.from_dsl_file(path)
RubyBuilder.generate_ruby_from_umm_project(uml)
