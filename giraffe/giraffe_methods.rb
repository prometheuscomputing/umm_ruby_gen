require_relative 'class_methods'
require_relative 'instance_methods'
require_relative 'query'
module Giraffe
  module GiraffeMethods
    def self.extended(base)
      base.extend(Giraffe::ClassMethods)
      base.include(Giraffe::InstanceMethods)
      
      # must come after extend ClassMethods
      orient_name = base.uml.orient_name      
     # These could be written without the String eval but would end up requiring an additional calls on the stack.
      base.module_eval """
        def self.giraffe
          Giraffe.db.get_class('#{orient_name}')
        end
        def self.orient_name
          '#{orient_name}'
        end
      """
    end
  end  
end
