require 'rainbow'
require 'uml_metamodel'
require 'appellation'
require_relative 'uml_metamodel'
require_relative 'type_mapping'
require_relative 'giraffe_methods'
require 'pp'

module Giraffe
  def self.db
    @db
  end
  def self.db=(db)
    @db = db
  end
  def self.connect(opts)
    @db.connect(opts)
  end
end
