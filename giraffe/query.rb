require_relative 'filters/filters'
module Giraffe
  def self.instantiate_result(result, from, opts = {})
    return nil unless result
    # puts "*"*22
    # puts from.orient_types
    # pp result
    result = result['result'].first if result.is_a?(Hash) && result['result'] # Maybe too lenient.
    type = from.orient_types[result['@class']]
    ruby_instance = type.new
    ruby_instance.giraffe = result
    from.attributes.each_value do |info|
      val = result[info[:getter].to_s]
      ruby_instance.send("#{info[:getter]}=".to_sym, val) if val
    end
    return ruby_instance
  end
  
  def self.instantiate_results(results, from, opts = {})
    results = results['result'] if results.is_a?(Hash) && results['result']
    results.collect{ |result| instantiate_result(result, from) }
  end
  
  def self.get_by_id(id)
    Giraffe.db.command("SELECT * FROM #{id}")['result'].first
  end
  
  class Query
    def self.execute(inst)
      Giraffe.db.command(inst.query)
    end
      
    attr_accessor :query
    def initialize(type: :select, from:, select: {})
      # TODO implement selections
      @from  = from # from must be a constant
      @where = false
      @query = case type
      when :select
        'SELECT ' + 'FROM ' + @from.orient_name
      else
        raise "#{type} queries are not yet supported!"
      end
    end
    # def select(from, *columns)
    #   new('SELECT ' + columns.collect { |c| c.to_s }.join(', ') + ' FROM ' + from.to_s)
    # end
    def first
      @query << ' LIMIT 1'
      result = Query.execute(self)&.[]('result')&.first
      Giraffe.instantiate_result(result, @from)
    end
    def limit(val)
      @query << ' LIMIT ' + val.to_s
      results = Query.execute(self)
      Giraffe.instantiate_results(results, @from)
    end
    def all
      # puts Rainbow(@query).orange
      results = Query.execute(self)
      # puts Rainbow("___________ ALL _____________").yellow
      Giraffe.instantiate_results(results, @from)
    end
  end
end