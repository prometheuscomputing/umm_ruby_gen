module UmlMetamodel
  class ElementBase
    attr_reader :implementation
    def prepare_for_giraffe(builder)
      # puts "Preparing #{self.class} -- #{qualified_name}"
      builder.register(self)
    end
  end
  
  class Element < ElementBase
  end
  
  class Project < ElementBase
    def prepare_for_giraffe(builder)
      contents.each { |e| e.prepare_for_giraffe(builder) }
    end
    def create_giraffe
      create_ruby
    end
  end
  
  class Package < Element
    
    def prepare_for_giraffe(builder)
      super
      contents.each { |e| e.prepare_for_giraffe(builder) }
    end
    
    def create_giraffe
      create_ruby
    end
  end
  
  class Classifier < Element
    def prepare_for_giraffe(builder)
      super
      contents.each { |e| e.prepare_for_giraffe(builder) } # Not sure what this will actually get.  Inner classes?
      properties.each { |e| e.prepare_for_giraffe(builder) } unless primitive? # while primitives may have properties that take part in associations, they do not get implemented
    end
    
    def giraffe
      Giraffe.db.get_class(orient_name)
    end
    
    def create_giraffe
      mod = create_ruby
      opts = {}
      opts[:abstract] = true if (abstract? || interface?)

      if is_a?(UmlMetamodel::Class) && association_class_for
        # Not supporting inheritence / polymorhpism on assoc classes right now.  We could do so.
        opts[:extends] = 'E'
      else
        extensions = (parents + interfaces).collect(&:orient_name)
        extensions << 'V' if extensions.empty?
        opts[:extends] = extensions.join(", ")
      end

      Giraffe.db.create_class(orient_name, opts)
      mod.extend(Giraffe::GiraffeMethods)
    end
    
    def create_giraffe_properties
      # FIXME iterate through immediate properties and properties of immediately implemented interfaces
      props = properties# + interfaces.collect(&:properties) # note that this call to properties is on the UmlMetamodel instance and will only get the properties defined on that classifier.  It isn't like #properties on the implementation which will get info about all of the properties of the classifier
      # HACK to deal with an upstream problem where an Enumeration that inherits from another enumeration can end up with two 'value' properties.  OrientDB will not allow you to define a property on a class if a superclass already defines a property with that name.
      if enumeration? && parents.any?
        props = props.reject { |prop| prop.name == 'value' }
      end
      props.each do |prop|
        prop.create_ruby
        # FIXME If we do this for composites then we don't have a link from composed to composer!  Maybe use an edge instead?  Same holds true for any attribute typed as a non-primitive that would be implmented with a link.
        if prop.association# && !prop.composite?
          assoc_giraffe = prop.association.create_giraffe
          # FIXME Finish This!!
          # puts "property: #{prop.name} -- #{Giraffe.db.get_class(prop.association.orient_name)}"
          # unless assoc_giraffe
          #   puts;puts prop.to_dsl
          #   puts;puts prop.association.orient_name
          # end
          # key = (prop.name || Appellation.role(prop)).to_sym
          # implementation.update_property(key, { :edge => assoc_giraffe['name'], :orient_class => prop.type.orient_name })
        else
          prop.create_giraffe
        end
      end
      
    end
    
    def create_giraffe_assoc_methods(property, edge, direction)
      role = (property.name || Appellation.role(property))
      if property.upper > 1
      else
      end
      implementation.module_eval """
        # def #{role}(*args)
        #   get_property(:#{role}, *args)
        # end
        
        # def #{role}=(val)
        #   set_property(:#{role}, val)
        # end
        #
        # def add_#{role}(val)
        #   add_to_collection(:#{role}, val)
        # end
        #
        # def remove_#{role}
        #   remove_rom_collection(:#{role}, val)
        # end
      """
    end
      
    
    def orient_name
      @orient_name ||= qualified_name.gsub('::', '__')
    end
  end

  class Class < Classifier
  end
  
  class Interface < Classifier
  end
  
  class Primitive < Datatype
    def create_giraffe
      # override super.  We don't need to create classes for primitive types in OrientDB.
    end
  end
  
  # class Datatype < Classifier
  # end
  
  # class Enumeration < Datatype
  # end
  
  class Property < Element
    def prepare_for_giraffe(builder)
    end
    
    def create_giraffe
      opts = {}
      # FIXME min and max can't be options if the type is DATE.  There may be other restrictions too.
      # opts[:min] = lower
      # opts[:max] = upper unless upper == Float::INFINITY

      # opts[:default] = default_value if default_value # FIXME must have things like DATE formatted correctly to make OrientDB happy
      args = [owner.giraffe['name']]
      if type.primitive?
        orient_type = Giraffe.orient_primitive_for(type)
        raise "#{type.qualified_name} does not map to an OrientDB type." unless orient_type
        args << name
        if upper > 1
          if is_unique
            args << "EMBEDDEDSET #{orient_type}"
          else
            args << "EMBEDDEDLIST #{orient_type}"
          end
        else
          args << orient_type
        end
      else # then it isn't a primitive and we will create a link instead
        args << (name || Appellation.role(self))
                
        if type.implementation.class == Module # in which case it is a mixin / interface.  TODO: is there a better way to support interfaces in OrientDB?
          linked_class = 'V'
        else
          linked_class = type.giraffe['name']
        end
        
        if upper > 1
          if is_unique
            args << "LINKSET #{linked_class}"
          else
            args << "LINKLIST #{linked_class}"
          end
        else
          args << :link
          opts[:linked_class] = linked_class
        end
      end
      args << opts
      Giraffe.db.create_property(*args)
    end    
  end
  
  class Association < Element
    def prepare_for_giraffe(builder, opts = {})
      super(builder)
    end
    def giraffe
      association_class ? Giraffe.db.get_class(association_class.orient_name) : super
    end
    def create_giraffe
      # association_class creation is handled in Classifier#create_giraffe
      if association_class || Giraffe.db.class_exists?(orient_name)
        # Do we need to do anything in this case?
      else
        opts = {}
        opts[:extends] = 'E'
        Giraffe.db.create_class(orient_name, opts)
      end
      edge = Giraffe.db.get_class(orient_name)
      Giraffe.db.command("ALTER CLASS #{orient_name} CUSTOM `from`='#{properties.first.qualified_name}'")
      Giraffe.db.command("ALTER CLASS #{orient_name} CUSTOM `to`='#{properties.last.qualified_name}'")
      set_property_metadata(properties.first, :from, edge)
      set_property_metadata(properties.last, :to, edge)
      properties.first.owner.create_giraffe_assoc_methods(properties.first, edge, :from)
      properties.last.owner.create_giraffe_assoc_methods(properties.last, edge, :to)
      edge
    end
    
    def set_property_metadata(property, direction, edge)
      getter = (property.name || Appellation.role(property)).to_sym
      args = { :edge => edge['name'] }
      args[:from] = true if direction == :from
      args[:to]   = property.type.orient_name
      property.owner.implementation.update_property(getter, args)
    end
    private :set_property_metadata
    
    def orient_name
      if association_class
        @orient_name ||= association_class.orient_name
      else
        @orient_name ||= (name ? qualified_name : package.qualified_name + '__' + Appellation.association_name(self)).gsub('::', '__')
      end
    end
  end
  
  # class EnumerationLiteral < Element
  # end
end
