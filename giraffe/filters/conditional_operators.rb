module Giraffe
  module Expressions
    def expressions(exp, args = {})
      return if args.empty?
      args.collect { |field, val| expression(exp, field, val) }.join(' AND ')
    end
    def expression(expression, field, val)
      return unless expression && field
      "#{field} #{expression} '#{val}'"
    end
    
    # Not implemented from OrientDB: IS, BETWEEN, INSTANCEOF
    # Mainly trying to cover those that work when applied to any type of property or are exclusive to strings
    operator_methods = [
      # [:is, '='], # special handling below
      [:like, 'LIKE'],
      [:<, '<'],
      [:<=, '<='],
      [:>, '>'],
      [:>=, '>='],
      [:!=, '!='],
      # [:between, 'BETWEEN'],
      [:in, 'IN'],
      [:contains_text, 'CONTAINSTEXT'],
      [:matches, 'MATCHES']
    ]
    
    operator_methods.each do |name, operation|
      instance_eval """
        def #{name}(args = {})
          expression('#{operation}', args)
        end
      """
    end
    
    def is(args = {})
      return if args.empty?
      exps = args.collect do |field, val|
        if val.nil?
          expression('IS', field, 'NULL')
        else
          expression('=', field, val)
        end
      end
      exps.join(' AND ')
    end
    
    # def self.like(args = {})
    #   expression('LIKE', args)
    # end
    #
    # def self.contains(args = {})
    #   expression('CONTAINS', args)
    # end
    #
    # def self.matches(args = {})
    #   expression('CONTAINS', args)
    # end
  end
  extend Expressions
end
