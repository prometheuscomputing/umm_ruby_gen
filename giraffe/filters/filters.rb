require_relative 'conditional_operators'

module Giraffe
  class Query
    def or(expression)
      @query << ' OR ' + expression
      self
    end
    
    # def and
    #   @query << ' AND '
    #   self
    # end
    
    # def add_where
    #   unless @where
    #     @where = true
    #     @query << ' WHERE '
    #   end
    # end
    # private :add_where
    
    def where(filter)
      @query << ' WHERE '
      if filter.is_a?(Hash)
        addition = Giraffe.is(filter)
      else
        addition = filter
      end
      @query << addition
      self
    end
    
    def exclude(filter)
      @query << ' WHERE NOT'
      if filter.is_a?(Hash)
        addition = Giraffe.is(filter)
      else
        addition = filter
      end
      @query << filter + ')'
      self
    end

  end

  class QueryExpression
    def initialize
    end
  end
end