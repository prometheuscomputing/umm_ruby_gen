require 'rainbow'
require 'umm_driven_builder'
require 'orientdb4r'
require_relative 'giraffe'
require 'common/constants'

class GiraffeBuilder < UMMDrivenBuilder
  
  def self.generate_from_umm_project(project)
    new(project).build
  end
    
  def initialize(project)
    super
  end

  def build
    @project.prepare_for_giraffe(self)
    # puts @registry.pretty_inspect"
    order_packages
    order_classifiers
    @ordered_elements.each do |e|
      # puts "#{e.class} -- #{e.qualified_name}"
      e.create_giraffe if e.respond_to?(:create_giraffe)
    end
    @ordered_elements.each do |e|
      next unless e.is_a?(UmlMetamodel::Classifier)
      next if e.is_a?(UmlMetamodel::Interface) # FIXME make sure this is how you want to handle this
      next if e.is_a?(UmlMetamodel::Primitive)
      e.create_giraffe_properties
    end
  end
end