require 'date'
require 'uri'

=begin
OrientDB property types:

copied from: https://orientdb.com/docs/last/SQL-Create-Property.html on Feb. 22, 2019

  OrientDB supports the following data types for standard properties:

  BOOLEAN	SHORT	DATE	  DATETIME	BYTE
  INTEGER	LONG	STRING	LINK	    DECIMAL
  DOUBLE	FLOAT	BINARY	EMBEDDED	LINKBAG

  It supports the following data types for container properties.

  EMBEDDEDLIST	EMBEDDEDSET	EMBEDDEDMAP
  LINKLIST	    LINKSET	    LINKMAP

=end


module Giraffe
  def self.primitive_mapping(reload = false)
    @umm_to_orient_primitive_mapping ||= {}
    @umm_to_orient_primitive_mapping = {} if reload
    return @umm_to_orient_primitive_mapping unless @umm_to_orient_primitive_mapping.empty?
    
    @umm_to_orient_primitive_mapping['UML::Integer'] = 'INTEGER'
    @umm_to_orient_primitive_mapping['UML::Real'] = 'FLOAT'
    @umm_to_orient_primitive_mapping['UML::String'] = 'STRING'
    @umm_to_orient_primitive_mapping['UML::Boolean'] = 'BOOLEAN'
    @umm_to_orient_primitive_mapping['UML::UnlimitedNatural'] = 'INTEGER'

    @umm_to_orient_primitive_mapping['UML::ByteString'] = 'BYTE'
    @umm_to_orient_primitive_mapping['UML::Char'] = 'STRING'
    @umm_to_orient_primitive_mapping['UML::Date'] = 'DATE'
    @umm_to_orient_primitive_mapping['UML::Float'] = 'FLOAT'
    # FIXME OrientDB does not support properties typed as Null / Nil.  Using STRING is arbitrary
    @umm_to_orient_primitive_mapping['UML::Null'] = 'STRING'
    @umm_to_orient_primitive_mapping['UML::Datetime'] = 'DATETIME'
    @umm_to_orient_primitive_mapping['UML::Time'] = 'DATETIME'
    # There are no built-in types in OrientDB specifically for URIs or RegularExpressions
    @umm_to_orient_primitive_mapping['UML::Uri'] = 'STRING'
    @umm_to_orient_primitive_mapping['UML::RegularExpression'] = 'STRING'

    # yucky hacks because GBP stuff is yucky...
    @umm_to_orient_primitive_mapping['Gui_Builder_Profile::Date'] = 'DATE'
    @umm_to_orient_primitive_mapping['Gui_Builder_Profile::Time'] = 'DATETIME'
    @umm_to_orient_primitive_mapping['Gui_Builder_Profile::Timestamp'] = 'DATETIME'
    @umm_to_orient_primitive_mapping['Gui_Builder_Profile::BinaryData'] = 'BINARY'
    @umm_to_orient_primitive_mapping['Gui_Builder_Profile::RegularExpression'] = 'STRING'
    # BigString needs to go away.  Don't ever start allowing it here.  returning nil should result in an exception later on, which is a good thing.
    @umm_to_orient_primitive_mapping['Gui_Builder_Profile::BigString'] = nil

    @umm_to_orient_primitive_mapping
  end
  def self.orient_primitive_for(uml_primitive)
    base = uml_primitive.base_primitive || uml_primitive
    # raise "#{uml_primitive.qualified_name} does not have a base_primitive" unless base
    primitive_mapping[base.qualified_name]
  end  
end
