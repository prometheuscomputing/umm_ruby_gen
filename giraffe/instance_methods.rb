module Giraffe
  module InstanceMethods
    def giraffe
      @giraffe
    end
    def giraffe=(val)
      @giraffe = val
    end
    def id
      giraffe&.[]("@rid")
    end
    def save
      ret = nil
      if @giraffe
        # pp @giraffe
        attrs_to_set = {}
        self.class.attributes.each_key do |getter|
          val = send(getter)
          attrs_to_set[getter] = val unless val == @giraffe[getter.to_s]
        end
        if attrs_to_set.any?
          cmd = "UPDATE #{self.class.orient_name} SET "
          cmd << attrs_to_set.collect { |k, v| "#{k} = '#{v}'" }.join(', ')
          cmd << " WHERE @rid='#{id}'"
          @giraffe = (Giraffe.db.command cmd)['result'].first
          ret = self
        end        
      else
        cmd = "CREATE VERTEX #{self.class.orient_name}"
        attrs_to_set = {}
        self.class.attributes.each_value do |info|
          val = send(info[:getter])
          attrs_to_set[info[:getter]] = val if val 
        end
        if attrs_to_set.any?
          cmd << ' SET '
          cmd << attrs_to_set.collect { |k, v| "#{k} = '#{v}'" }.join(', ')
        end
        @giraffe = (Giraffe.db.command cmd)['result'].first
         # TODO probably need error handling here
        # puts;puts cmd
        # pp @giraffe
        ret = self
      end
      self.class.associations.each do |getter, info|
        val = send(getter)
        # I think this is one place where concurrency checking might happen...
        if info[:type] == :one_to_one || info[:type] == :many_to_one
          next if val == get_property(getter)
        else
          # FIXME I'm really not sure this is right...
          next if [val].flatten.compact == get_property(getter)
        end            
        ret ||= self
        set_property(getter, val, :info => info)
      end      
      ret
    end
    
    # I don't like the method signature at all but I'm going by what SSA has.  I would much prefer to just use an options hash for all the optional stuff...keyword args are nice.
    def get_property(role, filter = {}, limit = nil, offset = 0) #return_association_hash = false, order = {} # not fooling with this stuff yet...
      role = role.to_sym
      info = get_info(role)
      # pp info
      return get_attribute(role, info) if info[:attribute]
      director = info[:from] ? "-#{info[:edge]}->" : "<-#{info[:edge]}-"
      cmd = "MATCH { Class: #{self.class.orient_name}, where: (@rid = '#{id}') }#{director}{ Class: #{info[:to]}, as: result } RETURN result"
      results = Giraffe.db.command(cmd)
      giraffes = results['result'].collect { |result| Giraffe.get_by_id(result['result'])}
      # puts Rainbow(giraffes.pretty_inspect).orange
      objs = Giraffe.instantiate_results(giraffes, info[:class].to_const)
    end
    
    # info is necessary for all the extra stuff that SSA does...
    def get_attribute(getter, info = nil)
      # for now...
      send(getter.to_sym)
    end
    
    def set_property(role, argument, opts = {})
      role = role.to_sym
      info = opts[:info] || get_info(role)
      # pp info
      return set_attribute(role, argument) if info[:attribute]
      current_entry = get_property(role)
      if current_entry
        # FIXME blow away current entry? Or reuse Edge?
      end
      # pp info
      if info[:type] == :one_to_one || info[:type] == :many_to_one
        add(role, argument, :info => info)
      else
        unless argument.is_a?(Array)
          pp info
          raise "#{self.class}##{role} is a to-many association.  #{self.class}##{role} must take an array.  You gave it #{argument.inspect}"
        end
        # puts "Adding to #{self.class} for a #{type} association using #{adder_method}: #{argument}"
        return_vals = argument.collect { |model| add(role, model, :info => info) } if argument
      end
    end
    
    def add(role, obj, options = {})
      # role should already be a symbol
      return if obj.nil?
      info = options[:info] || get_info(role)
      # TODO What if there is already an edge b/w these two objects?  Are collections really ordered sets? Or do we allow duplicates?
      # For now we are just making duplicates
      # pp info
      # in_out = info[:from] ? 'out' : 'in'
      # cmd = "MATCH {Class: #{info[:edge]}, as: the_edge, where: (#{in_out} = '#{id}')} RETURN the_edge"
      # # puts Rainbow(cmd).orange
      # edge = Giraffe.db.command(cmd)['result'].first
      # if edge
      #   if info[:from]
      #     cmd = "UPDATE #{edge['@rid']} SET out = #{id}, in = #{obj.id}"
      #   else
      #     cmd = "UPDATE #{edge['@rid']} SET out = #{obj.id}, in = #{id}"
      #   end
      # else
        cmd = "CREATE EDGE #{info[:edge]} FROM "
        if info[:from]
          cmd << "#{id} to #{obj.id}"
        else
          cmd << "#{obj.id} to #{id}"
        end
      # end
      edge = Giraffe.db.command(cmd)['result'].first
    end
    private :add # maybe shouldn't be private but it is potentially dangerous to expose this.
    
    def set_attribute(getter, val)
      # for now...
      send(getter.to_sym, val)
    end
    
    def get_info(key, fail_silently = false)
      info = self.class.properties[key.to_sym]
      # info = self.class.info_for(getter.to_sym) # uncomment for debugging
      unless info || fail_silently
        message = "#{self.class} does not have property: #{key.inspect}\n#{self.class} has the following property keys: #{self.class.properties.keys.sort.inspect}"
        raise message
      end
      info
    end
      
      
    # def _get_opp_info(getter, fail_silently = false)
    #   ::SpecificAssociations.get_opposite_association(_get_info(getter))
    # end
    # private :_get_opp_info
    
  end
end
