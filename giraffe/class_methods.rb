module Giraffe
  module ClassMethods
    # Perhaps dangerous to do caching / lazy initialization
    def orient_types
      return @orient_types if @orient_types
      if uml.is_a?(UmlMetamodel::Interface)
        umls = uml.all_children.unshift(uml).collect(&:implementors).collect(&:all_children).uniq
      else
        umls = uml.all_children.unshift(uml)
      end
      @orient_types = umls.reject(&:abstract?).collect { |model| [model.orient_name, model.implementation] }.to_h
    end
    
    def reset_orient_types
      @orient_types = nil
      orient_types
    end
    
    def where(args = {})
      # FIXME implement filters!
      q = Giraffe::Query.new(:type => :select, :from => self)
      q.where(args)
    end
    def first
      @select.call.first
      # Giraffe::Query.new(:type => :select, :from => self).first
    end
    def limit(val)
      Giraffe::Query.new(:type => :select, :from => self).limit(val)
    end
    def all
      Giraffe::Query.new(:type => :select, :from => self).all
    end
    def [](val)
      raise "[] is not implemented on #{self.class}"
      if val.is_a?(Symbol)
        puts val.inspect
      end
      if val.to_s =~ /\d+:\d+/
        puts val.inspect
      end 
    end
    
    # This is the equivalent to adding class instance variables to Ruby classes where the values must be Strings.
    def giraffe_metadata
      giraffe['custom']
    end
    
    def giraffe_properties
      giraffe['properties']
    end
    
    def giraffe_property(getter)
      giraffe_properties.find { |entry| entry['name'] == getter.to_s }
    end
    
    def add_metadata(property, value)
      Giraffe.db.command("ALTER CLASS #{orient_name} CUSTOM `#{property}`='#{value}'")
    end
    
  end  
end
