require 'rspec'
require 'lodepath'
LodePath.amend
LodePath.display
require 'uml_metamodel'
require_relative '../lib/ruby_builder'
require_relative '../giraffe/builder'

def rspec_setup
  path = File.expand_path('~/projects/uml_metamodel/test_data/CarExampleApplication.rb')
  @uml = UmlMetamodel.from_dsl_file(path)
  # TODO try using binary client Orientdb4r.client()
  @db_name = 'rspec'
  @db_cred = 'root'
  Giraffe.db = Orientdb4r.client
  @db = Giraffe.db
  @db_args = {:database => @db_name, :user => @db_cred, :password => @db_cred}
  existing_dbs = @db.list_databases(:user => @db_cred, :password => @db_cred)
  if existing_dbs.include?(@db_name)
    @db.delete_database(@db_args)
  end
  @db.create_database(@db_args.merge({:storage => :plocal}))# , :type => :graph) # :storage => :plocal
  @db.connect(@db_args)
  @test_class = 'TestClass'
  expect(@db.get_class('OUser')).not_to be_nil # getting a built in class lets us know that we are connected and can query
  if @db.class_exists?(@test_class)
    expect(@db.drop_class(@test_class)["result"]).to eq [{"className"=>"TestClass", "operation"=>"drop class"}]
  end
  expect(@db.class_exists?(@test_class)).to be false
  expect(@db.database_exists?(@db_args)).to be true
end

def rspec_teardown
  if @db.class_exists?(@test_class)
    expect(@db.drop_class(@test_class)["result"]).to eq [{"className"=>"TestClass", "operation"=>"drop class"}]
  end
  @db.disconnect
  @db.delete_database(@db_args)
  expect(@db.list_databases(@db_args.reject { |k,_| k == :database })).not_to include @db_name
end