require_relative 'spec_helper'

describe 'ruby_gen functionality' do
  
  before(:all) do
    rspec_setup
  end

  after(:all) do
    rspec_teardown
  end
  
  it 'tests that the setup and teardown are alright' do
    expect(true).to eq true
  end
  
  it 'should be able to create and delete classes' do
    @db.create_class(@test_class)
    test_class = @db.get_class(@test_class)
    expect(test_class).to be_a Hash
    expect(test_class["name"]).to eq @test_class
    @db.drop_class(@test_class)
    expect(@db.class_exists?(@test_class)).to be false
  end
end
  
describe 'Linking Ruby classes to OrientDB classes' do
  before(:all) do
    rspec_setup
    GiraffeBuilder.generate_from_umm_project(@uml)
  end
  after(:all) do
    rspec_teardown
  end

  it 'should create the properties hashes for all domain classes' do
    props = People::Person.properties
    expect(props[:name][:getter]).to eq :name
    expect(props[:founded][:getter]).to eq :founded
  end
  
  it 'should be able to create an OrientDB class with properties' do
    g = People::Person.giraffe
    expect(g).to be_a Hash
    expect(g["name"]).to eq "People__Person"
    weight = g["properties"].find { |prop| prop['name'] == 'weight'}
    expect(weight).not_to be_nil
    expect(weight['type']).to eq 'FLOAT'
  end
  
  it 'UmlMetamodel classes and associations know what their corresponding OrientDB class is named' do
    uml = People::Person.uml
    expect(uml.orient_name).to eq 'People__Person'
    assoc = uml.properties.find { |prop| prop.name == 'occupying' }.association
    expect(assoc.orient_name).to eq 'Automotive__PersonOccupying_VehicleOccupants'
    # assoc class and assoc both map to the same Edge
    assoc = uml.properties.find { |prop| prop.name == 'drives' }.association
    expect(assoc.orient_name).to eq 'People__Driving'
    expect(assoc.association_class.orient_name).to eq 'People__Driving'
  end
  
  it 'association metadata should reference Edge classes (i.e. OrientDB classes that are subclasses of E)' do
    props = People::Person.properties
    expect(props[:founded][:edge]).to eq 'People__Founding'
    expect(props[:founded][:edge]).to eq 'People__Founding'
    props = Geography::City.properties
    expect(props[:founder][:edge]).to eq 'People__Founding'
  end
  
  it 'Saving new instances of domain classes should result in persistance in the database and the ability to retrieve them from the database' do
    person1 = People::Person.new
    person1.name = 'Fred'
    person2 = People::Person.new
    person2.name = 'Bob'
    clown = People::Clown::Clown.new
    clown.name = 'Bozo'
    
    person1.save
    person2.save
    clown.save
    
    bob = People::Person.where(:name => 'Bob').first
    expect(bob.class).to eq People::Person
    expect(bob.name).to eq 'Bob'
    
    all_people = People::Person.all
    expect(all_people.count).to eq 3
    bozo = all_people.find { |x| x.class == People::Clown::Clown }
    expect(bozo.name).to eq 'Bozo'
    
    person1.name = 'Simon'
    person1.save
  end
  
  it 'a Ruby instance can report id and the result of the last time a query to retrieve it from the database was performed' do
    # This stuff should be in a test for testing saving and stuff instead of in the the assoc test
    person1 = People::Person.new
    expect(person1.giraffe).to be_falsey
    expect(person1.id).to be_falsey
    person1.save
    expect(person1.giraffe['@class']).to eq 'People__Person' 
    expect(person1.id).to match(/#\d+:\d+/)
  end
  
  # NOTE -- This test is currently dependent on the previous test passing.
  it 'Associations work' do
    person1 = People::Person.new
    person1.name = 'Bubba'
    car1 = Automotive::Car.new
    car1.save
    van1 = Automotive::Minivan.new
    van1.save

    person1.drives = [car1, van1]
    person1.save
    
    bubba_cars = person1.get_property(:drives)
    expect(bubba_cars.collect(&:class)).to contain_exactly(Automotive::Car, Automotive::Minivan)
    
  end
  
  it 'classes can access the OrientDB property metadata and custom metadata' do
    name_prop = People::Person.giraffe_property('name')
    expect(name_prop['type']).to eq 'STRING'
    
    People::Person.add_metadata('testify', 'Hallelujah')
    People::Person.add_metadata('goombah', 'Harumph')
    metadata = People::Person.giraffe_metadata
    expect(metadata['testify']).to eq 'Hallelujah'
    expect(metadata['goombah']).to eq 'Harumph'
  end
  
end
