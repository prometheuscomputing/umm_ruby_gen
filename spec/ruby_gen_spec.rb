require_relative 'spec_helper'

describe 'ruby_gen functionality' do
  path = File.expand_path('~/projects/uml_metamodel/test_data/CarExampleApplication.rb')
  uml  = UmlMetamodel.from_dsl_file(path)
  RubyBuilder.generate_ruby_from_umm_project(uml)
  
  it 'should be able to instantiate Ruby modules and classes from UmlMetamodel instances' do
    # Not comprehensive but you get the idea. Could be made more comprehensive
    expect(People.uml.class).to eq UmlMetamodel::Package
    expect(Automotive::Warrantied.uml.class).to eq UmlMetamodel::Interface
    expect(Automotive::Dollar.uml.class).to eq UmlMetamodel::Primitive
    expect(People::Handedness.uml.class).to eq UmlMetamodel::Enumeration
    expect(People::Handedness.uml.literals.first.name).to eq "Ambidextrous"
    expect(People::Person.uml.class).to eq UmlMetamodel::Class
    expect(People::Person.methods).to include :name
    expect(People::Clown.methods).to include :name
    # because #name is defined on Person
    expect(People::Clown.instance_methods(false)).not_to include :name
    # #methods doesn't return setters defined by attr_accessor...
    bob = People::Person.new
    bob.name = 'bob'
    expect(Automotive::Vehicle.ancestors).to include Automotive::Warrantied
  end
  
  it 'UmlMetamodel packages and classifiers should have be able to reference their implementations' do
    uml_class = UmlMetamodel.find('People::Person', uml)
    expect(uml_class.implementation).to eq People::Person
    uml_package = UmlMetamodel.find('People', uml)
    expect(uml_package.implementation).to eq People
    uml_impl = UmlMetamodel.find('Automotive::Warrantied', uml)
    expect(uml_impl.implementation).to eq Automotive::Warrantied
  end
  
  # NOTE -- In the old SSA based API, the #implements methods was used to set up the relationship between an interface and the classifier that implements it.  This was a bad name for the method.  A better name would have been #implement! because it is present tense and it modifies the receiver.  #implements is a good name for asking the question, "What does this classifier implement," and that is how it is used here.  Note also that, thus far, we don't need any sort of #implements! method.
  # NOTE also that the UmlMetamodel meaning of #implements is more consistent with what is used here.
  # NOTE also that in this API we are just getting the interfaces that are directly implemented.  If you want all of the implementors you can get that by working with #ancestors
  it 'should be able to determine the interfaces that a class directly implements' do
    expect(Automotive::Warrantied.implements).to eq [Automotive::HasSerial, Automotive::HasWarranty]
    expect(People::Person.implements).to eq []
    expect(Automotive::Dollar.implements).to eq []
    expect(Automotive::Vehicle.implements).to eq [Automotive::Warrantied]
  end
  
  it 'should manage metadata' do
    expect(People::Person.info.class).to eq Hash
    expect(People::Person.properties.keys).to include :name
    expect(People::Person.properties[:name][:as]).to eq "People::Person"
    expect(People::Clown::Clown.properties.keys).to include :name
    expect(People::Clown::Clown.properties[:name][:as]).to eq "People::Person"
    expect(People::Person.properties[:drives][:association]).to be true
    expect(People::Person.properties[:drives][:type]).to eq :many_to_many
    expect(People::Person.properties[:founded][:type]).to eq :one_to_one
    expect(Automotive::Mechanic.properties[:employer][:type]).to eq :many_to_one
    # pp Automotive::RepairShop.properties
    expect(Automotive::RepairShop.properties[:mechanics][:type]).to eq :one_to_many
  end
end
